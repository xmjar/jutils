package cn.xmjar.utils.dao;

import java.util.List;
import java.util.Set;

/**
 * <li>定义公共接口</li>
 * @author 欣茂Java网络课堂开源组件 https://xmjar.cn
 * @version 1.0.0
 * @url 腾讯课堂学编程：https://xmjar.ke.qq.com/
 * @param <K> 表示对象主键
 * @param <V> 表示对象
 */
public interface IDAOUtils<K,V> {
    /**
     * 加入登录检测方法
     * @param vo 登录成功返回对象，失败返回null
     * @return
     */
    public V findLogin(K vo);
    /**
     * <li>对象数据添加</li>
     * @param vo 表示对象
     * @return 增加成功返回true，增加失败返回false
     */
    public boolean doCreate(V vo);

    /**
     * <li>更新对象数据，根据对象主键更新</li>
     * @param id 表示对象主键
     * @return 更新成功返回true，更新失败返回false
     */
    public boolean doUpdate(K id);

    /**
     * <li>根据编号删除行数据</li>
     * @param id 删除对象数据，要删除的对象数据编号
     * @return 删除成功返回true，删除失败返回false
     */
    public boolean doRemove(K id);

    /**
     * <li>批量删除数据，使用set集合保存数据</li>
     * @param ids 表示要执行删除的id集合数据
     * @return 删除成功返回true，删除失败返回false
     */
    public boolean doRemoveBatch(Set<K> ids);

    /**
     * <li>查询对象中的所有数据</li>
     * @return 查询成功返回所有记录数，查询失败返回null
     */
    public List<V> findAll();

    /**
     * <li>数据分页与模糊查询</li>
     * @param keyWord 要执行查询的关键字
     * @param column 要执行查询的列
     * @param currentPage 当前页
     * @param lineSize 每页显示记录数
     * @return 查询成功返回满足条件的记录数，查询失败返回null
     */
    public List<V> findAllBySplit(String keyWord,String column,Integer currentPage,Integer lineSize);

    /**
     * <li>模糊查询并且统计数据量</li>
     * @param keyWord 要执行查询的关键字
     * @param column 要执行查询的列
     * @return 查询成功返回记录数，查询失败返回0
     */
    public Integer getAllCount(String keyWord,String column);

    /**
     * <li>数据分页查询</li>
     * @param currentPage 当前页
     * @param lineSize 每页显示记录数
     * @return 查询成功返回满足条件的记录数，查询失败返回null
     */
    public List<V> findAllBySplit(Integer currentPage,Integer lineSize);

    /**
     * <li>统计数据量</li>
     * @return 查询成功返回记录数，查询失败返回0
     */
    public Integer getAllCount();

    /**
     * <li>通过编号查询对象表中的数据</li>
     * @param id 要查询的编号
     * @return 查询成功返回该行数据，查询失败返回null
     */
    public V findById(K id);
}
